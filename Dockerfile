FROM debian:bookworm

RUN apt-get update && apt-get -y install \
    texlive-full \
    biber \
    rubber

# create user with a home directory
RUN mkdir /work
WORKDIR /work

COPY entrypoint.sh /entrypoint.sh

ENV TEX_TARGET=""

ENTRYPOINT ["/entrypoint.sh"]
