# latex-rubber

`latex-rubber` is a container image for compiling latex document with rubber and texlive.
It installs `texlive-full`, `biber` and `rubber`, and can be used in your CI pipelines to automatically compile latex documents.

Warning: the image is huge in size (~), mainly because `texlive-full` is huge.

## Run

If your latex files are located in the `<tex_file_folder>`, with its main file named `<main.tex>`, run:

```sh
docker run -e TEX_TARGET=<main.tex> -v <tex_file_folder>:/work pthevenet/latex-rubber
podman run -e TEX_TARGET=<main.tex> -v <tex_file_folder>:/work pthevenet/latex-rubber
```

You will find the generated `main.pdf` file in the same `<tex_file_folder>`.

## Gitlab-CI

```yml
variables:
  IMAGE: registry.gitlab.com/pthevenet/latex-rubber:latest

stages:
  - build

build-tex:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker pull $IMAGE
    - docker run -e TEX_TARGET=main.tex -v ${PWD}:/work $IMAGE || true
  artifacts:
    paths:
      - main.pdf
```

## Build


```sh
podman build --tag mytag -f .

# because the image is large, you may need to change the container engine tmpdir
# with podman: add --tmpdir=...
# with docker: export TMPDIR=/mypath
```